import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";

class ForumList extends Component {
	static propTypes = {};

	state = {
		activeIndex: -1
	};

	constructor(props) {
		super(props)
	}

	switchToActionClass(index, ev) {
		// console.log(431, ev);
		// ev.target.currentTarget.name="active";
		if (ev) {
			ev.preventDefault();
		}
		if (this.state.activeIndex === index) {
			return this.setState({activeIndex: -1});
		}
		this.setState({activeIndex: index});
		// console.log("active is", this.state.active)
	}

	render() {
		const {items = []} = this.props;

		return (
			<div>
				{
					items.map((item, index) => {

						return <a href="#" key={index} onTouchTap={this.switchToActionClass.bind(this, index)}
											className={`list-group-item list-group-item-action list-group-item-dark list-group-item-dark
							 d-flex justify-content-between align-items-center ${index === this.state.activeIndex ? 'active' : ''}`}
						>  {item.title} <span className="badge badge-primary badge-pill">{item.badge}</span>
						</a>

					})
				}
			</div>
		)
	}


}

const mapStateToProps = (state) => {
	return {}
};

const MapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(ForumList);
