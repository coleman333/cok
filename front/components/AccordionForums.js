import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";


class AccordionForums extends Component {
    static propTypes = {};

    state = {};

    constructor(props) {
        super(props);
    }

    makeid() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    render() {
        const {items=[], accordionId} = this.props;

        return (
            <div id={`accordion${accordionId}`}>
                {
                    items.map((item, index)=> {
                        let heading = this.makeid();
                        let collapse = this.makeid();
                        return <div className="card" key={index}>
                            <div className="card-header" id={`${heading}${index}`}>
                                <h5 className="mb-0">
                                    <button className="btn btn-link" data-toggle="collapse"
                                            data-target={`#${collapse}${index}`}
                                            aria-expanded="false" aria-controls={`${collapse}${index}`}>
                                        {item.title}
                                    </button>
                                </h5>
                            </div>

                            <div id={`${collapse}${index}`} className="collapse" aria-labelledby={`${heading}${index}`}
                                 data-parent={`#accordion${accordionId}`}>
                                <div className="card-body">
                                    {item.text}
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(AccordionForums);



