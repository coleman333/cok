import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import AccordionForums from './AccordionForums';
import GydroZoneCards from './GydroZoneCards';
import FontIcon from 'material-ui/FontIcon';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import SvgIcon from 'material-ui/SvgIcon';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import ElectroZoneCards from '../components/ElectroZoneCards'
import loadCardsAction from '../actions/loadCardsAction'
import StroyZoneCards from '../components/StroyZoneCards'
import stroyIcon from '../../images/hydroIcon2.svg'
import electroIcon from '../../images/electroIcon.svg'
import hydroIcon from '../../images/drop.svg'
import ForumList from './ForumList';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import forumAction from '../actions/forumAction';

const dialogStyle = {
	width: '80%',
	maxWidth: 'none',
};

class TabTemplate extends Component {
    static propTypes = {};

    state = {
        slideIndex: 1,
			cardLimit:0,
			skipCards:10,
      open:false
    };

    constructor(props) {
        super(props);
    }

    tabChange = (value) => {
        this.setState({
            slideIndex: value,
        });
    };

    // makeid() {
    //     let text = "";
    //     let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		//
    //     for (var i = 0; i < 5; i++) {
    //         text += possible.charAt(Math.floor(Math.random() * possible.length));
    //     }
		//
    //     return text;
    // }


	  componentDidMount(){
        this.props.loadCardsAction.loadCardsMainPage(this.state.cardLimit,this.state.skipCards)
          .then(() => {
				// this.props.cards.reverse();
				// this.forceUpdate();
			})

    }

	openModalAddNewForum = (category) => {
		this.setState({open: true});
		this.setState({category:category})
		console.log(category)
	};

	closeModalAddNewPost = () => {
		// this.setState({open: false});
		// console.log(1234,this.props.cards);
		this.setState({
			open: false,
			// organisation: '',
			// signUnderPicture: '',
			// CardTitle: '',
			// text: '',
			// textUnderPicture: '',
			// CardSubtitle: '',
			// category: '',
			// fileContent: null
		});
		// this.state.file = null;
		// console.log(moment().format('MMMM Do YYYY, h:mm:ss '))
	};

	formFields(fieldName, value) {
		// console.log(1234,value.target.value);
		this.setState({[fieldName]: value.target.value});
	}
    addNewForum(){
			const newForum = {};
			this.setState({BadgeAmountMessages:'0'});
			// this.openModalAddNewPost();
			if (this.state.title) {
				newForum.title = this.state.title;
			}
			if (this.state.category) {
				newForum.category = this.state.category;
			}
			newForum.idUser = this.props.user._id;
			newForum.avatar = this.props.user.avatar;
			newForum.firstName = this.props.user.firstName;
			newForum.BadgeAmountMessages = this.state.BadgeAmountMessages;

			console.log('asdf',newForum.idUser);

			this.props.forumAction.addNewForum(newForum)
				.then(() => {
					this.setState({open: false});
						// this.props.forumAction.loadForums(this.props.user._id.toString(), this.state.forumLimit, this.state.skipForums)
						// 	.then(() => {
						//
						// 	});
					 this.setState({title :''});
					// this.state.signUnderPicture = '';


				})
    }

    render() {
			const dialogButtons = [
				<FlatButton
					label="Cancel"
					primary={true}
					onClick={this.closeModalAddNewPost.bind(this)}
				/>,
				<FlatButton
					label="Submit"
					primary={true}
					keyboardFocused={true}
					onClick={this.addNewForum.bind(this)}
          // onRequestClose={this.closeModalAddNewPost}
				/>
			];
        const hydroForumsList=[{title:'add new forum topic',badge:3},{title:'add new forum ',badge:32},
          {title:'new forum ',badge:13}];
			const electroForumsList=[{title:'add new forum topic',badge:56},{title:'add new forum ',badge:32},
				{title:'new forum ',badge:26}];
			const stroyForumsList=[{title:'add new forum topic',badge:4},{title:'add new forum ',badge:56},
				{title:'new forum ',badge:43}];
        const {hydroZoneCards,countHydroZoneCards,electroZoneCards,countElectroZoneCards,stroyZoneCards,countStroyZoneCards
        } = this.props.allCards;
        return (
            <div className="row">
                <Tabs onChange={this.tabChange} value={this.state.slideIndex}  className="col-md-12"
											tabItemContainerStyle={{backgroundColor:'#616263'}}
											inkBarStyle={{backgroundColor:'red',height:'4px'}}
								>
                    <Tab label="общайся" value={0}
										/>
                    <Tab label="читай" value={1} />
                    <Tab label="находи и покупай" value={2} />
                </Tabs>
                <SwipeableViews index={this.state.slideIndex} onChangeIndex={this.tabChange}
                                style={{width:'100%',fontWeight: 400 }}>
                    <div style={{paddingTop: 5,display:'flex', justifyContent:'center'}}>

                        <div className="row text-center" style={{width:'100%'}}>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="hydro-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={hydroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
                                />
                                <div className="row">
																	<div className="list-group col-md-12 " style={{paddingLeft:'15px'}}>
																		<a href="#"
																			 className="list-group-item list-group-item-action list-group-item-dark list-group-item-dark"
                                    onTouchTap={this.openModalAddNewForum.bind(this,'hydroZone')}>
																			add new forum</a>
																		<ForumList items={hydroForumsList} />
																	</div>
                                </div>

                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="electro-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={electroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                                <div className="row">
                                      <div className="list-group col-md-12 " style={{paddingLeft:'15px'}}>
																				<a href="#"
																					 className="list-group-item list-group-item-action list-group-item-dark list-group-item-dark"
																					 onTouchTap={this.openModalAddNewForum.bind(this,'electroZone')}>
																					add new forum</a>
																				<ForumList items={electroForumsList} />

																			</div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="stroy-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={stroyIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                                <div className="row">
																	<div className="list-group col-md-12 " style={{paddingLeft:'15px'}}>
																		<a href="#"
																			 className="list-group-item list-group-item-action list-group-item-dark list-group-item-dark"
																			 onTouchTap={this.openModalAddNewForum.bind(this,'stroyZone')}>
																			add new forum</a>
																		<ForumList items={stroyForumsList} />

																	</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style={{paddingTop:5,display:'flex', justifyContent:'center'}}>
                        <div className="row " style={{width:'100%'}}>
                            <div className="col-md-4">
                                <RaisedButton style={{ marginBottom:'10px',width:'100%' }}
                                              label="hydro-zone"
                                              labelPosition="before"
                                              primary={true}
                                              icon={<img src={hydroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                                <GydroZoneCards hydroZoneCards={hydroZoneCards } />
                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="electro-zone"
                                              labelPosition="before"
                                              primary={true}
                                              icon={<img src={electroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                                <ElectroZoneCards electroZoneCards={electroZoneCards}/>
                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="stroy-zone"
                                              labelPosition="before"
                                              primary={true}
                                              icon={<img src={stroyIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                                <StroyZoneCards stroyZoneCards={stroyZoneCards}/>
                            </div>
                        </div>
                    </div>
                    <div style={{paddingTop:5,display:'flex', justifyContent:'center'}}>
                        <div className="row " style={{width:'100%'}}>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%'}}
                                              label="hydro-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={hydroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
                                />
                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="electro-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={electroIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                            </div>
                            <div className="col-md-4">
                                <RaisedButton style={{marginBottom:'10px',width:'100%' }}
                                              label="stroy-zone"
                                              labelPosition="before"
                                              primary={true}
																							icon={<img src={stroyIcon} alt=""/>}
																							buttonStyle={{backgroundColor:'#616263'}}
																/>
                            </div>
                        </div>
                    </div>
                </SwipeableViews>
							<Dialog
								title="create new forum"
								actions={dialogButtons}
								modal={true}
								open={this.state.open}
								contentStyle={dialogStyle}
								autoScrollBodyContent={true}
							>
								<div className="row">
									<div className="col-xs-12 col-md-12">
										<div className="row">
											<div className="col-md-12">
												<TextField hintText="title of the forum"
																	 floatingLabelText="title of the forum"
																	 onChange={this.formFields.bind(this, 'title')}
												/>
											</div>
										</div>
									</div>
									<b className="d-block d-sm-none">aaaaaaaaaaaaaaaa</b>

								</div>
							</Dialog>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
			allCards:state.getIn(['cardReducer','allCards']).toJS(),
			countAllCards: state.getIn(['cardReducer', 'countAllCards']),
			user: state.getIn(['auth', 'user']).toJS(),
			forums:state.getIn(['forumReduser','forums'])
		}
};

const MapDispatchToProps = (dispatch) => {
    return {
			loadCardsAction: bindActionCreators(loadCardsAction, dispatch),
			forumAction: bindActionCreators(forumAction, dispatch)

		}
};

export default connect(mapStateToProps, MapDispatchToProps)(TabTemplate);

//
//  items2={hidroCards }
// items2={electroCards}
// items2={stroyCards}
