import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import user2 from '../../images/user2.png'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {browserHistory} from 'react-router';

class GydroZoneCards extends Component {
    static propTypes = {};

    state = {};

    constructor(props) {
        super(props);
        
    }

    // redirectToFullCard(){
    //     browserHistory.push('/fullReadCard')
    // }

	onCardPush(idUser,idCard){
		browserHistory.push(`/cabinet/${idUser}/${idCard}`);
	}

    render() {
        const {hydroZoneCards=[]} = this.props;

        return (
           <div>
               {
								 hydroZoneCards.map((item,index)=>{
                       
                    return <div key={index} style={{marginBottom:10}}>
                       <Card  onTouchTap={this.onCardPush.bind(this,item.idUser,item._id)} style={{backgroundColor:'#c2c4c6'}}>
                           <CardHeader
                               title={`${item.firstName}   ${item.datePosted}`}
                               subtitle={item.organisation}
                               avatar={item.avatar}
                           />
                           <CardMedia
                               overlay={<CardTitle title={item.signUnderPicture} subtitle={item.textUnderPicture} />}
                           >
                               <img src={item.file} alt="" style={{height:'400px'}}/>
                           </CardMedia>
                           <CardTitle title={item.CardTitle} subtitle={item.CardSubtitle}/>
                           <CardText>
                             {item.text}
                           </CardText>
                       </Card>
                    </div>
                   })
               }
           </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(GydroZoneCards);




