import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import user2 from '../../images/user2.png'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {browserHistory} from 'react-router';

class FullReadCard extends Component {
    static propTypes = {};

    state = {};

    constructor(props) {
        super(props);

    }




    render() {
        const {items=[]} = this.props;

        return (
            <div>
                         <div  style={{marginBottom:10}}>
                            <Card >
                                <CardHeader
                                    title="URL Avatar"
                                    subtitle="Subtitle"
                                    avatar={user2}
                                />
                                <CardMedia
                                    overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
                                >
                                    <img src="images/dog.jpg" alt=""/>
                                </CardMedia>
                                <CardTitle title="Card title" subtitle="Card subtitle"/>
                                <CardText>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                                    Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed
                                    pellentesque.
                                    Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                                </CardText>
                                <CardActions>
                                    <FlatButton label="Action1"/>
                                    <FlatButton label="Action2"/>
                                </CardActions>
                            </Card>
                        </div>
                    })

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(FullReadCard);





