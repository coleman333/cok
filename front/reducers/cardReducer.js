import {createReducer} from 'redux-act';
import {fromJS} from 'immutable';
import utils from '../utils';
import addNewPost from '../actions/addNewPostCabinet';
import loadCardsAction from '../actions/loadCardsAction';
import loadMoreCards from '../actions/loadMoreCards';

const initialState = fromJS({
	processing: 0,
	error: {},
	value: '',
	cards: [],
	countCards: 0,
	allCards:[],
	countAllCards:0
});

export default createReducer({


	[addNewPost.addNewPost.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[addNewPost.addNewPost.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1)
		return state.update('cards', cards => cards.push(fromJS(payload.response.data)))
	},
	[addNewPost.addNewPost.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	},

	[loadCardsAction.loadCards.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[loadCardsAction.loadCards.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1);//1#{processing:1}
		// state= state.update('countCards', countCards => fromJS(payload.response.data.countCards))
		return state
			.set('countCards', fromJS(payload.response.data.countCards))//2#{processing:1,countCards:100}
			.set('cards', fromJS(payload.response.data.cards))//3#{processing:1,countCards:100,cards:[]}
	},
	[loadCardsAction.loadCards.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	},

	[loadMoreCards.loadMoreCardsAction.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[loadMoreCards.loadMoreCardsAction.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1)
		return state.update('cards', cards => cards.concat(fromJS(payload.response.data.cards)))
			.set('countCards', fromJS(payload.response.data.countCards))
	},
	[loadMoreCards.loadMoreCardsAction.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	},

	[loadCardsAction.loadCardsMainPage.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[loadCardsAction.loadCardsMainPage.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1);
		// console.log('qwerty',payload.response.data.hydroZoneCards);
		return state
			.set('countAllCards', fromJS(payload.response.data.countCards))
			.set('allCards', fromJS(payload.response.data.cards))
	},
	[loadCardsAction.loadCardsMainPage.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	},

	[loadCardsAction.redirectToUserCard.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[loadCardsAction.redirectToUserCard.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1);
		// console.log('qwerty',payload.response.data.hydroZoneCards);
		return state
			// .set('countAllCards', fromJS(payload.response.data.countCards))
			.set('cards', fromJS(payload.response.data.cards))
	},
	[loadCardsAction.redirectToUserCard.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	}

}, initialState);

