import {createReducer} from 'redux-act';
import {fromJS} from 'immutable';
import utils from '../utils';
import actionForum from '../actions/actionForum';


const initialState = fromJS({
	processing: 0,
	error: {},
	value: '',
	forums: [],
	countForums: 0,

});

export default createReducer({


	[actionForum.addNewForum.request]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') + 1)
	},
	[actionForum.addNewForum.ok]: (state, payload) => {
		state = utils.setProcessingToState(state, state.get('processing') - 1)
		return state.update('forums', forums => forums.push(fromJS(payload.response.data)))
	},
	[actionForum.addNewForum.error]: (state, payload) => {
		return utils.setProcessingToState(state, state.get('processing') - 1)
	}

}, initialState);

