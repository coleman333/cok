import React from 'react';
import {Route, IndexRoute, IndexRedirect} from 'react-router';
import MainPage from './containers/MainPage';
import Edition from './containers/Edition'
import Cabinet from './containers/Cabinet'
import TabTemplate from './components/TabTemplate'
import FullReadCard from './components/FullReadCard';

export default (store) => {
	function requireAuth(nextState, replace, callback) {
		const authUser = store.getState().getIn(['auth', 'user']);
		if (authUser.isEmpty()) {
			replace({
				pathname: '/',
				// state:{nextPathname:nextState.location.pathname}
			});
		}
		callback();
	};
	// const redirectUser = (nextState, replace , callback)=>{
	//     const authUser = store.getState().getIn(['auth','user']);
	//     if(authUser.isEmpty()){
	//         replace({
	//             pathname:'/'
	//         });
	//     }
	//     callback();
	// };
	return (
		<Route>
			<Route path="/" component={MainPage}>
				<IndexRoute component={TabTemplate}/>
				<Route onEnter={requireAuth}>
					<Route path="edition" component={Edition}/>
					{/*<Route path="cabinet" component={Cabinet}/>*/}
					<Route path="fullReadCard" component={FullReadCard}/>

				</Route>
					<Route path="cabinet/:idUser" component={Cabinet}/>
				<Route path="login" component="div"/>


			</Route>
		</Route>
	)
}


// <Route path ="login" component="div"/>
// {
//     // <Route path="/login" component={Login} onEnter={redirectAuth}/>                    
// }