import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import "../assets/styles/main.scss";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {browserHistory} from 'react-router';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router';
import Registration from './Registration';
import Login from './Login';

import authActions from '../actions/auth';
import IconButton from 'material-ui/IconButton';
import logIn from '../../images/logIn.svg';
import enter from '../../images/enter.svg';
import edit from '../../images/edit.svg';
import user2 from '../../images/user2.png'
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import SvgIcon from 'material-ui/SvgIcon';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';
// import AccordionForums from '../components/AccordionForums';

import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import IconLocationOn from 'material-ui/svg-icons/communication/location-on';

const recentsIcon = <FontIcon className="material-icons">restore</FontIcon>;
const favoritesIcon = <FontIcon className="material-icons">favorite</FontIcon>;
const nearbyIcon = <IconLocationOn/>;

import TabTemplate from '../components/TabTemplate';
import IconMenu from 'material-ui/IconMenu';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import DropDownMenu from 'material-ui/DropDownMenu';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'
import AppBar from 'material-ui/AppBar';
import screw2 from '../../images/screw2.png';
import TextField from 'material-ui/TextField';
import search from '../../images/search.svg';


class MainPage extends Component {
	static propTypes = {};

	state = {
		open: false,
		openRegistration: false,
		// messages: [],
		openLogin: false,
		// slideIndex: 1,
		selectedBottomIndex: 0
	};

	constructor(props) {
		super(props);

		// this.state.slideIndex=0;
		// this.state = Object.assign({}, this.state, {slideIndex: 0});

	}

	// const styles = {
	//     headline: {
	//         fontSize: 24,
	//         paddingTop: 16,
	//         marginBottom: 12,
	//         fontWeight: 400,
	//     },
	//     slide: {
	//         padding: 10,
	//     },
	// };

	handleOpen = () => {
		this.setState({open: true});
	};

	onRegistrationToggle(value) {
		this.setState({
			openRegistration: value
		})
	}

	handleToggle(ev) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		// this.state.open=true;
		// this.forceUpdate()
		this.setState({
			open: !this.state.open
		})
	}

	// handleOpen = () => {
	//     this.setState({open: true});
	// };
	// goTo(path) {
	//     browserHistory.push(path);
	//     this.setState({
	//         open: false
	//     })
	// }

	goToCabinet() {
		browserHistory.push(`/cabinet/${this.props.user.get('_id')}`);
	}


	onLoginToggle(value) {
		this.setState({
			openLogin: value
		})
	}

	closeDrawerToggle() {
		this.setState({
			open: false
		})
	}

	logoutToggle() {
		this.props.authActions.logout()
			.then((res) => {
				// this.handleClose();
				browserHistory.push('/')
				if (this.props.onSubmitSuccess) {
					this.props.onSubmitSuccess(ev);
				}
				//close the panel
				// console.log('ok', res)
				this.closeDrawerToggle();

			})
			.catch(err => {
				console.log(err)

			})
	}

	select = (index) => this.setState({selectedBottomIndex: index});

	// col row justify-content-end
	// style={{display:'flex', justifyContent:'flex-end'}}
	// <label htmlFor="" key="1">личный кабинет</label>,
// <FontIcon className="muidocs-icon-custom-github" />

	// openEdit(){
	//     browserHistory.push('/edition')
	//
	// }

	// <div className="navbar navbar-expand-lg navbar-dark bg-dark">
//
// </div>

	render() {

		const {
			children, user
		} = this.props;

		let content;
		if (!user.isEmpty()) {
			content = <MenuItem onTouchTap={this.logoutToggle.bind(this)}>Logout</MenuItem>;
			console.log(11, 'event')
		} else {
			content = [
				<MenuItem key="1" onTouchTap={this.onLoginToggle.bind(this, true)}>Login</MenuItem>,
				<MenuItem
					key="2"
					onTouchTap={this.onRegistrationToggle.bind(this, true)}>Registration</MenuItem>
			]
		}

		return (
			<MuiThemeProvider>
				<div className="container-fluid">
					<div style={{marginTop: '10px', marginBottom: '10px'}}>
						<Toolbar  style={{backgroundColor:'#969696'}}>
							<ToolbarGroup firstChild={true}>
								<div><img src="../../images/logoBlack.svg" alt="" style={{width: '45px', margin: '10'}}/></div>
								<div style={{fontSize: '36px', margin: '10px'}}>profi-zone</div>
								<div style={{display: 'flex', justifyContent: 'flex-end'}}>
									<div style={{
										borderStyle: 'solid', borderColor: ' Gray', borderRadius: '5',
										margin: '10px', height: '47px'
									}}>
										<TextField
											hintText="Hint Text"
											style={{marginLeft: '10px', marginRight: '10px'}}
										/>

									</div>
									<div style={{
										borderStyle: 'solid',
										borderColor: 'Gray',
										borderRadius: '5',
										height: '47',
										marginTop: '10'
									}}>
										<img src={search} style={{width: '24px', height: '24', margin: '3px', marginTop: '10px'}}/>
									</div>
								</div>
							</ToolbarGroup>
							<ToolbarGroup>
								<div className="row justify-content-end">

									<div className="" style={{display: 'flex', justifyContent: 'flex-end'}}>
										{
											!this.props.user.isEmpty() &&
											[
												<RaisedButton target="_blank" label="личный кабинет" key="1"
																			onTouchTap={this.goToCabinet.bind(this)}
																			style={{marginBottom: '10px', marginTop: '10px'}}
																			primary={true}
																			icon={<SvgIcon color={greenA200} hoverColor={red500}>
																				<path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/>
																			</SvgIcon>}
																			buttonStyle={{backgroundColor:'#616263'}}
												/>,
												<RaisedButton key="3" className="customIconBtn" label={user.get('firstName')}
																			primary={true}
																			icon={<img src={user.get('avatar')}/>}
																			style={{margin: '10px'}}
																			buttonStyle={{backgroundColor:'#616263'}}
												/>
											]
										}
									</div>

								</div>
								<ToolbarSeparator/>
								<div className="" style={{display: 'flex', justifyContent: 'center'}}>
									<IconButton className=" "
															tooltip="REGISTER OF LOGIN"
															tooltipPosition="bottom-left"
															onTouchTap={this.handleToggle.bind(this)}>
										<img src={logIn}/>
									</IconButton>
								</div>
							</ToolbarGroup>
						</Toolbar>
					</div>


					{children}

					<Drawer open={this.state.open} onRequestChange={() => console.log(123)}>
						{content}
						<MenuItem onTouchTap={this.closeDrawerToggle.bind(this)}>Cancel</MenuItem>
					</Drawer>

					<Registration
						onSubmitSuccess={this.handleToggle.bind(this)}
						onRegistrationToggle={this.onRegistrationToggle.bind(this)}
						open={this.state.openRegistration}
					/>
					<Login
						onSubmitSuccess={this.handleToggle.bind(this)}
						onLoginToggle={this.onLoginToggle.bind(this)}
						open={this.state.openLogin}
					/>

				</div>
			</MuiThemeProvider>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		user: state.getIn(['auth', 'user'])
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		authActions: bindActionCreators(authActions, dispatch)
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);


//
// <Paper zDepth={1}>
//     <BottomNavigation selectedBottomIndex={this.state.selectedBottomIndex}>
//         <BottomNavigationItem
//             label="Recents"
//             icon={recentsIcon}
//             onClick={() => this.select(0)}
//         />
//         <BottomNavigationItem
//             label="Favorites"
//             icon={favoritesIcon}
//             onClick={() => this.select(1)}
//         />
//         <BottomNavigationItem
//             label="Nearby"
//             icon={nearbyIcon}
//             onClick={() => this.select(2)}
//         />
//     </BottomNavigation>
// </Paper>