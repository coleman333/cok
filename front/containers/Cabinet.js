import React, {Component} from "react";
import {connect} from "react-redux";
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import SelectField from 'material-ui/SelectField';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import dog from '../../images/dog.jpg';
import ActionInfo from 'material-ui/svg-icons/action/info';
import {browserHistory} from 'react-router';
import MobileTearSheet from '../components/MobileTearSheet';
import {List, ListItem} from 'material-ui/List';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {pinkA200, transparent} from 'material-ui/styles/colors';
import Subheader from 'material-ui/Subheader';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
// import pictureExample from '../../images/deadline5.svg';
import {bindActionCreators} from 'redux';
import addNewPostCabinet from '../actions/addNewPostCabinet';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
// import ActionFavorite from 'material-ui/svg-icons/action/favorite';
// import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import loadCardsAction from '../actions/loadCardsAction';
import loadMoreCards from '../actions/loadMoreCards';
import moment from 'moment'

const dialogStyle = {
	width: '80%',
	maxWidth: 'none',
};


class Cabinet extends Component {
	static propTypes = {};

	state = {
		slideIndex: 1,
		open: false,
		text: "There is the place for your content",
		category: 'electoroZone',
		// posts:[]
		space: "									",
		cardLimit: 0,
		skipCards: 10
	};

	constructor(props) {
		super(props);
	}

	componentDidMount() {

		// let url = window.location.href;
// console.log(1111,this.props.location.query.card)
// console.log(2222,this.props.params.idUser)
		// let result = url.slice(url.indexOf('cabinet/'),url.length).split('/');
		// const idUser = result[1];
		// const idCard = result[2];
		const idCard = this.props.location.query.card;
		const idUser = this.props.params.idUser;
		// location.hash ='http://localhost:3005/cabinet';
		// window.history.pushState(null,null,'http://localhost:3005/cabinet');
		// console.log(7654,idUser,idCard);
		if (idCard) {
			this.props.loadCardsAction.redirectToUserCard(idUser, idCard,)
				.then(() => {
					let element1 = document.getElementById(idCard);
					let scrollToThisPlace = element1.offsetTop;
					document.body.scrollTop = scrollToThisPlace;
				})
		}
		else {
			this.props.loadCardsAction.loadCards(this.props.user._id.toString(), this.state.cardLimit, this.state.skipCards)
				.then(() => {
					// this.props.cards.reverse();
					// this.forceUpdate();
				})
		}
	}

	loadMorePosts() {
		this.props.loadMoreCards.loadMoreCardsAction(this.props.user._id.toString(), this.state.cardLimit + 10, this.state.skipCards)
			.then(() => {

				this.setState({amountCards: this.props.countCards})
				console.log(this.props.cards);
			})
	}

	// showCards(counterCards){
	// 	// for(let i = counterCards;i>=0;i--){
	// 	// 	if(this.props.cards[i] != undefined)
	// 		 // this.state.cards.push(this.props.cards[i]);
	// 	// }
	// 	// this.forceUpdate()
	// 	this.setState({cards:_.takeRight(this.props.cards,counterCards)})
	// 	// this.setState({cards:this.state.cards2});
	// 	console.log('sdfh sdfh sdfh sdh',this.state.cards)
	// }

	openModalAddNewPost = () => {
		this.setState({open: true})

	};

	closeModalAddNewPost = () => {
		// this.setState({open: false});
		// console.log(1234,this.props.cards);
		this.setState({
			open: false,
			organisation: '',
			signUnderPicture: '',
			CardTitle: '',
			text: '',
			textUnderPicture: '',
			CardSubtitle: '',
			category: '',
			fileContent: null
		});
		// this.state.file = null;
		// console.log(moment().format('MMMM Do YYYY, h:mm:ss '))
	};

	AddNewPost() {
		const newPost = {};

		if (this.state.organisation) {
			newPost.organisation = this.state.organisation;
		}
		if (this.state.fileContent && this.state.signUnderPicture) {
			newPost.signUnderPicture = this.state.signUnderPicture;
		}
		if (this.state.fileContent && this.state.textUnderPicture) {
			newPost.textUnderPicture = this.state.textUnderPicture;
		}
		if (this.state.CardTitle) {
			newPost.CardTitle = this.state.CardTitle;
		}
		if (this.state.CardSubtitle) {
			newPost.CardSubtitle = this.state.CardSubtitle;
		}
		if (this.state.text) {
			newPost.text = this.state.text;
		}
		if (this.state.selectedPicture) {
			newPost.file = this.state.selectedPicture;
			newPost.fileType = this.state.fileType;
		}
		if (this.state.category) {
			newPost.category = this.state.category;
		}

		newPost.idUser = this.props.user._id;
		newPost.avatar = this.props.user.avatar;
		newPost.firstName = this.props.user.firstName;

		this.props.addNewPostCabinet.addNewPost(newPost)
			.then(() => {
				this.setState({open: false});
				this.props.loadCardsAction.loadCards(this.props.user._id.toString(), this.state.cardLimit, this.state.skipCards)
					.then(() => {
						// this.showCards(this.state.counterCards)	;
						// this.props.cards.reverse();
						// this.forceUpdate();
						console.log(this.props.cards);
					});
				this.state.organisation = '';
				this.state.signUnderPicture = '';
				this.state.CardTitle = '';
				this.state.text = '';
				// this.state.file = null;
				this.state.textUnderPicture = '';
				this.state.CardSubtitle = '';
				this.state.category = '';
				this.state.fileType = '';

			})
	}


	formFields(fieldName, value) {
		// console.log(1234,value.target.value);
		this.setState({[fieldName]: value.target.value});
		// switch(flag){
		// case 1:
		//   this.setState({subTitle:value.target.value});
		//   // this.state.subTitle = value.target.value;
		//     break;
		// case 2:
		// 	this.setState({OverlayTitle:value.target.value});
		// 	break;
		// case 3:
		// 	this.setState({OverlaySubtitle:value.target.value});
		//   break;
		// case 4:
		// 	this.setState({CardTitle:value.target.value});
		//   break;
		// case 5:
		// 	this.setState({CardSubtitle:value.target.value});
		// 	break;
		// case 6:
		// 	this.setState({text:value.target.value});
		// 	break;
		// }
	}

	selectFile(ev) {
		const file = ev.target.files[0];
		this.getBase64(file)
			.then(content => {
				this.setState({fileContent: content, selectedPicture: file});
				// console.log('asdfasdf' ,this.state.fileContent);
				// console.log('fghjfghj' ,this.state.selectedPicture.type);
				// this.setState({type: this.state.selectedPicture.type.toString()}) ;
				const type = this.state.selectedPicture.type.toString();
				if (type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'image/x-icon'
					|| type == 'image/pjpeg' || type == 'application/pdf' || type == 'image/x-jg' || type == 'image/bmp'
					|| type == 'image/x-pict' || type == 'image/pict' || type == 'image/vnd.microsoft.icon' || type == 'image/x-pict') {
					this.setState({fileType: 'picture'});

				}
				if (type == 'video/x-ms-asf-plugin' || type == 'video/avi' || type == 'video/msvideo' || type == 'video/x-sgi-movie'
					|| type == 'video/mpeg' || type == 'video/x-mpeg' || type == 'video/x-mpeq2a' || type == 'video/x-mpeq2a' || type == 'video/x-mpeq2a'
					|| type == 'video/mp4' || type == 'video/quicktime' || type == 'video/mpeg' || type == 'video/mpeg' || type == 'video/mpeg') {
					this.setState({fileType: 'video'});

				}
				if (type == 'audio/aiff' || type == 'audio/x-aiff' || type == 'audio/x-jam' || type == 'audio/x-mpequrl' || type == 'audio/midi'
					|| type == 'audio/x-mid' || type == 'audio/x-midi' || type == 'audio/mod' || type == 'audio/x-mod' || type == 'audio/mpeg'
					|| type == 'audio/x-mpeg' || type == 'audio/mpeg3' || type == 'audio/x-mpeg-3' || type == 'audio/wav' || type == 'audio/wav') {
					this.setState({fileType: 'audio'});

				}
			})
			.catch(console.error.bind(console));
	}

	getBase64(file) {                                       //method convert pict into base 64 format
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
		});
	}

	handleChange = (value) => {
		this.setState({
			slideIndex: value,
		});
	};

	scrollToCard() {
		let element1 = document.getElementById('123');
		let scrollToThisPlace = element1.offsetTop;
		document.body.scrollTop = scrollToThisPlace;

	}

	render() {
		const {user} = this.props;
		const {cards} = this.props;
		const {countCards} = this.props;
		// const cardss = cards.reverse();

		// const {cards} = this.state.cards;
		// console.log(12341234, this.state.cards);
		const dialogButtons = [

			<FlatButton
				className="col-md-2"
				label="upload file"
				labelPosition="before"
				containerElement="label"
				style={{verticalAlign: 'middle'}}
			>
				<input type="file" style={{
					cursor: 'pointer',
					position: 'absolute',
					top: 0,
					bottom: 0,
					right: 0,
					left: 0,
					// width: '100%',
					opacity: 0
				}} onChange={this.selectFile.bind(this)}/>
			</FlatButton>,
			<FlatButton
				label="Cancel"
				primary={true}
				onClick={this.closeModalAddNewPost.bind(this)}
			/>,
			<FlatButton
				label="Submit"
				primary={true}
				keyboardFocused={true}
				onClick={this.AddNewPost.bind(this)}
				// onRequestClose={this.closeModalAddNewPost}
			/>
		];

		return (


			<div className="container-fluid">
				<div className="row">
					<div className="col-md-12">

						<Tabs
							onChange={this.handleChange}
							value={this.state.slideIndex}
						>
							<Tab label="Tab One" value={0}/>
							<Tab label="Tab Two" value={1}/>
							<Tab label="Tab Three" value={2}/>
							<Tab label="Tab Four" value={3}/>
						</Tabs>
						<SwipeableViews
							index={this.state.slideIndex}
							onChangeIndex={this.handleChange}
						>
							<div style={{padding: 10}}>

								<div className="col-md-3">
									<MobileTearSheet>
										<List>
											<Subheader>Recent chats</Subheader>
											<ListItem
												primaryText="Brendan Lim"
												leftAvatar={<Avatar src={dog}/>}
												rightIcon={<CommunicationChatBubble/>}
											/>
											<ListItem
												primaryText="Eric Hoffman"
												leftAvatar={<Avatar src={dog}/>}
												rightIcon={<CommunicationChatBubble/>}
											/>
											<ListItem
												primaryText="Grace Ng"
												leftAvatar={<Avatar src={dog}/>}
												rightIcon={<CommunicationChatBubble/>}
											/>
											<ListItem
												primaryText="Kerem Suer"
												leftAvatar={<Avatar src={dog}/>}
												rightIcon={<CommunicationChatBubble/>}
											/>
											<ListItem
												primaryText="Raquel Parrado"
												leftAvatar={<Avatar src={dog}/>}
												rightIcon={<ActionInfo/>}
											/>
										</List>
										<Divider/>
										<List>
											<Subheader>Previous chats</Subheader>
											<ListItem
												primaryText="Chelsea Otakan"
												leftAvatar={<Avatar src={dog}/>}
											/>
											<ListItem
												primaryText="James Anderson"
												leftAvatar={<Avatar src={dog}/>}
											/>
											<ListItem
												primaryText="создать новый пост"
												leftAvatar={<Avatar src={dog}/>}
												onTouchTap={this.AddNewPost.bind(this)}

											/>
										</List>
									</MobileTearSheet>
								</div>
							</div>
							<div style={{padding: 10}}>
								<div className="offset-md-3 col-md-6 ">
									<RaisedButton style={{marginBottom: '10px', width: '100%'}}
																label="add new post"
																labelPosition="before"
																primary={true}
																icon={<ActionAndroid/>}
																onTouchTap={this.openModalAddNewPost.bind(this)}
									/>
									<Dialog
										title="fill the form"
										actions={dialogButtons}
										modal={true}
										open={this.state.open}
										// onRequestClose={this.closeModalAddNewPost}
										contentStyle={dialogStyle}
										autoScrollBodyContent={true}
									>
										<div className="row">
											<div className="col-xs-12 col-md-6">
												<div className="row">
													<div className="col-md-12">
														<TextField hintText="organisation"

																			 floatingLabelText="organisation"
																			 onChange={this.formFields.bind(this, 'organisation')}
														/> <br/>
														<TextField hintText="sign of the picture"
																			 floatingLabelText="sign of the picture"
																			 onChange={this.formFields.bind(this, 'signUnderPicture')}
														/><br/>
														<TextField hintText="text under picture"
																			 floatingLabelText="text under picture"
																			 onChange={this.formFields.bind(this, 'textUnderPicture')}
														/> <br/>
														<TextField hintText="CardTitle"
																			 floatingLabelText="CardTitle"
																			 onChange={this.formFields.bind(this, 'CardTitle')}
														/><br/>
														nvc<TextField hintText="CardSubtitle"
																					floatingLabelText="CardSubtitle"
																					onChange={this.formFields.bind(this, 'CardSubtitle')}
													/><br/>
														<RadioButtonGroup
															name="zones"
															valueSelected={this.state.category}
															onChange={(ev, value) => this.setState({category: value})}
														>
															<RadioButton
																value="hydroZone"
																label="hydro Zone"
																// onChange={this.changeCategory.bind(this)}
															/>
															<RadioButton
																value="electoroZone"
																label="electoro zone"
																// onChange={this.changeCategory.bind(this)}
															/>
															<RadioButton
																value="stroyZone"
																label="stroy-zone"
																// onChange={this.changeCategory.bind(this)}
															/>
														</RadioButtonGroup>
														<TextField hintText="text"
																			 multiLine={true}
																			 rows={2}
																			 floatingLabelText="text"
																			 onChange={this.formFields.bind(this, 'text')}
														/>
													</div>
												</div>
											</div>
											<b className="d-block d-sm-none">aaaaaaaaaaaaaaaa</b>
											<div className="col-xs-12 col-md-6">
												<div className="row">
													<div className="col-md-12">

														<Card>
															<CardHeader
																title={user.firstName}
																subtitle={this.state.organisation}
																avatar={user.avatar}
															/>
															<CardMedia
																overlay={this.state.fileContent && this.state.signUnderPicture
																	? <CardTitle
																		title={this.state.signUnderPicture}
																		subtitle={this.state.textUnderPicture}/>
																	: undefined}
															>
																<div style={{}}>
																	{this.state.fileContent && this.state.fileType == 'picture' &&
																	<img src={this.state.fileContent}
																			 style={{width: '500px', margin: '20px', height: '300px'}}/>}
																	{this.state.fileContent && this.state.fileType == 'video' && <video
																		style={{width: '400px', height: '400px'}} controls="controls" preload="auto">
																		<source src={this.state.fileContent}/>
																	</video>}
																</div>
															</CardMedia>
															<CardTitle title={this.state.CardTitle}
																				 subtitle={this.state.CardSubtitle}/>
															<CardText>
																{this.state.text}
															</CardText>
														</Card>
													</div>
												</div>
											</div>
										</div>
									</Dialog>

								</div>
								<div className="container-fluid">
									<div className="row">
										<div className="offset-md-1 col-md-7">
											{
												cards.map((item, index) => {

													return <div key={index} style={{margin: 10}} id={item._id}>
														<Card style={{borderRadius: '5px'}}
															// onTouchTap={this.redirectToFullCard.bind(this)}
														>
															<CardHeader
																style={{display: 'flex'}}
																textStyle={{flex: 1, padding: 0}}
																title={<div style={{display: 'flex', justifyContent: 'space-between'}}>
																	<div>{item.firstName}</div>
																	<div>{item.datePosted}</div>
																</div>}
																subtitle={item.organisation}
																avatar={item.avatar}
															/>
															<CardMedia
																overlay={item.fileType !== 'video' || item.signUnderPicture ?
																	<CardTitle title={item.signUnderPicture}
																						 subtitle={item.textUnderPicture}/> : undefined}
															>
																<div>
																	{item.fileType === 'picture' && <img src={item.file} alt="" style={{
																		height: '400px',
																		width: '100%'
																	}}/>}
																	{item.fileType === 'video' && <video controls="controls" preload="auto">
																		<source src={item.file} style={{width: '100%', height: '500px'}}/>
																	</video>}
																</div>
															</CardMedia>
															<CardTitle title={item.CardTitle} subtitle={item.CardSubtitle}/>
															<CardText>
																{item.text}

															</CardText>
															<CardActions>

															</CardActions>
														</Card>
													</div>
												})
											}
											{cards.length > 9 && cards.length <= countCards && <div className="offset-md-2 col-md-8 ">
												<RaisedButton style={{marginBottom: '10px', width: '100%'}}
																			label="see more posts"
																			labelPosition="before"
																			primary={true}
																			icon={<ActionAndroid/>}
																			onTouchTap={this.loadMorePosts.bind(this)}
												/>

											</div>}
											<button onTouchTap={this.scrollToCard.bind(this)}>first top</button>

										</div>

										<div className="col-md-3">
											<div className="col-md-12" style={{
												borderRadius: '5px', border: '2px', borderStyle: 'solid',
												height: '500px', backgroundColor: 'white', margin: 30
											}}>
												<img src={dog} alt="" style={{margin: '10px', height: '400px', width: '300px'}}/>
												<span>there must be your contex advertisment</span>

											</div>
											<div className="col-md-12" style={{
												borderRadius: '5px', border: '2px', borderStyle: 'solid',
												height: '500px', backgroundColor: 'white', margin: 30
											}}>
												<img src={dog} alt="" style={{margin: '10px', height: '400px', width: '300px'}}/>
												<span>there must be your contex advertisment</span>
											</div>
											<div className="col-md-12" style={{
												borderRadius: '5px', border: '2px', borderStyle: 'solid',
												height: '500px', backgroundColor: 'white', margin: 30
											}}>
												<img src={dog} alt="" style={{margin: '10px', height: '400px', width: '300px'}}/>
												<span>there must be your contex advertisment</span>
											</div>
											<button id="123">scroll to this place</button>

										</div>

									</div>
								</div>
							</div>
							<div style={{padding: 10}}>
								slide n°3
							</div>
							<div style={{padding: 10}}>
								slide n°4
							</div>

						</SwipeableViews>
					</div>
				</div>
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		user: state.getIn(['auth', 'user']).toJS(),
		cards: state.getIn(['cardReducer', 'cards']).toJS(),
		countCards: state.getIn(['cardReducer', 'countCards']),
		// cards: state.getIn(['cardReducer', 'cards']).toJS()
	}
};

const MapDispatchToProps = (dispatch) => {
	return {
		addNewPostCabinet: bindActionCreators(addNewPostCabinet, dispatch),
		loadCardsAction: bindActionCreators(loadCardsAction, dispatch),
		loadMoreCards: bindActionCreators(loadMoreCards, dispatch),

	}
};

export default connect(mapStateToProps, MapDispatchToProps)(Cabinet);
