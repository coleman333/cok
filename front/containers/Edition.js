import React, {Component} from "react";
import {connect} from "react-redux";
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import SelectField from 'material-ui/SelectField';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
// import gif3 from '../images/gif3.gif';
// import gif2 from '../images/gif2.gif';
// import gif1 from '../images/gif1.gif';
import {browserHistory} from 'react-router';

class Edition extends Component {
    static propTypes = {
      
    };

    state = {
        premiumEditVar:'To edit the rate plan click here',
        editUser:'to edit user click here',
        createPremium:'to add new premium plan click here',
        createPaymentSystem:'to add new payment system'
    };

    constructor(props) {
        super(props);
    }

    editRatePlan(){
        // onTouchTap={editRatePlan.bind(this)}
        browserHistory.push('/editionPremium')
    }
    createRatePlan(){
        browserHistory.push('/createPremium')   
    }
    createPaymentSystem(){
        browserHistory.push('/createPaymentSystem');        
    }

    
    render(){
        return (
            <div>
                 <div className="premiumDiv" onClick={this.editRatePlan.bind(this)}>
                     <img className="premiumImg" src={gif3} alt=""/>
                     <div className="premiumTitle">{this.state.premiumEditVar}</div>
                 </div>
                <div className="premiumDiv" onClick={this.createRatePlan.bind(this)}>
                    <img className="premiumImg" src={gif3} alt=""/>
                    <div className="premiumTitle">{this.state.createPremium}</div>
                </div>
                <div className="premiumDiv" onClick={this.createPaymentSystem.bind(this)}>
                    <img className="premiumImg" src={gif2} alt=""/>
                    <div className="premiumTitle">{this.state.createPaymentSystem}</div>
                </div>
                <div className="premiumDiv" >
                    <img className="premiumImg" src={gif1} alt=""/>
                    <div className="premiumTitle">{this.state.editUser}</div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {}
};

const MapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, MapDispatchToProps)(Edition);
