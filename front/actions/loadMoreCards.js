import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

module.exports = {
	loadMoreCardsAction : createActionAsync('LOADMORECARDS',( idUser,cardLimit ,skipCards)=>{
		return axios ({
			method:'post',
			url:'api/cards/loadCards',
			data:{
				userId:idUser,
				cardLimit:cardLimit,
				skipCards:skipCards
			}
		})
	},{rethrow:true})


};