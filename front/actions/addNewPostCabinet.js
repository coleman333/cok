import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

module.exports = {
	addNewPost: createActionAsync('ADDNEWPOST', (newPost2) => {

		let newPost = new FormData();
		newPost2.organisation && newPost.append('organisation', newPost2.organisation);
		newPost2.signUnderPicture && newPost.append('signUnderPicture', newPost2.signUnderPicture);
		newPost2.textUnderPicture && newPost.append('textUnderPicture', newPost2.textUnderPicture);
		newPost2.CardTitle && newPost.append('CardTitle', newPost2.CardTitle);
		newPost2.CardSubtitle && newPost.append('CardSubtitle', newPost2.CardSubtitle);
		newPost2.text && newPost.append('text', newPost2.text);
		newPost2.idUser && newPost.append('idUser', newPost2.idUser);
		newPost2.file && newPost.append('file', newPost2.file);
		newPost2.category && newPost.append('category', newPost2.category);
		newPost2.avatar && newPost.append('avatar', newPost2.avatar);
		newPost2.firstName && newPost.append('firstName', newPost2.firstName);
		newPost2.fileType && newPost.append('fileType', newPost2.fileType);

		return axios({
			method: 'post',
			url: 'api/cards/addNewPost',
			data: newPost
		})
	}, {rethrow: true})


};