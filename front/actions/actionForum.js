import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

module.exports = {
	addNewPost: createActionAsync('ADDNEWFORUM', (newForum2) => {

		let newForum = new FormData();
		newForum2.organisation && newForum.append('organisation', newForum2.organisation);
		newForum2.title && newForum.append('text', newForum2.title);
		newForum2.idUser && newForum.append('idUser', newForum2.idUser);
		newForum2.avatar && newForum.append('avatar', newForum2.avatar);
		newForum2.firstName && newForum.append('firstName', newForum2.firstName);
		newForum2.role && newForum.append('role', newForum2.role);
		newForum2.category && newForum.append('category', newForum2.category);

		return axios({
			method: 'post',
			url: 'api/forums/addNewForum',
			data: newForum
		})
	}, {rethrow: true})


};