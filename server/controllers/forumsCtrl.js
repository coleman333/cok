var passport = require('passport');
var path = require('path');
var multer = require('multer');
var cardModel = require('../models/cardModel');
const forumModel = require('../models/forumModel');
const _ = require('lodash');

module.exports.upload = multer({
	storage: multer.diskStorage({
		destination: function (req, file, cb) {
			var absolutePath = path.join(__dirname, '../../images/');
			cb(null, absolutePath);
		},
		filename: function (req, file, cb) {
			cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
		}
	})
});


module.exports.addNewPost = function (req, res, next) {
	// console.dir(JSON.parse(req.body.user));
	//  req.body.user=JSON.parse(req.body);
	console.log(`\n\n=>>`, req.body);
	// console.dir(req.file);
	let newPost = req.body;
	// let user = req.body;

	newPost.datePosted = new Date();

	try {
		newPost.file = `/images/${req.file.filename}`; //'images'+req.file.filename
	} catch (ex) {
	}
	cardModel.create(newPost, function (err, newPost) {
		if (err) {
			console.error(err);
			return next(err);
		}
		res.json(newPost.toJSON());
// loadAllPost();

	});

};

module.exports.loadAllPosts = function (req, res, next) {
	const {userId, cardLimit, skipCards} = req.body;
	console.log(1234145345, userId, cardLimit, skipCards);

	cardModel.find({idUser: userId}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards).exec(function (err, cards) {
		if (err) {
			console.error(err);
			return next(err);
		}
		cardModel.count({idUser: userId}, function (err, countCards) {
			if (err) {
				console.log('mistake in count cards', countCards);
			}
			// console.log(idUser)
			console.log(countCards);
			return res.json({cards, countCards});
		});

	})
};

module.exports.loadCardsMainPage = function (req, res, next) {
	const {cardLimit, skipCards} = req.body;
	const cards = {};

	const HZCards = new Promise((resolve, reject) => {
		cardModel.find({category: 'hydroZone'}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards)
			.exec(function (err, hydroZoneCards) {
				if (err) {
					console.log('mistake in hydro cards', hydroZoneCards);
					console.error(err);
					return reject(err);
				}
				cards.hydroZoneCards = hydroZoneCards;
				resolve();
			})
	});
	const countHZCards = new Promise((resolve, reject) => {
		cardModel.count({category: 'hydroZone'}, function (err, countHydroZoneCards) {
			if (err) {
				console.log('mistake in count hydro cards', countHydroZoneCards);
				console.error(err);
				return reject(err);
			}
			cards.countHydroZoneCards = countHydroZoneCards;
			resolve();
		})
	});
	const EZCards = new Promise((resolve, reject) => {
		cardModel.find({category: 'electoroZone'}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards)
			.exec(function (err, electroZoneCards) {
				if (err) {
					console.log('mistake in electro cards', electroZoneCards);
					console.error(err);
					return reject(err);
				}
				cards.electroZoneCards = electroZoneCards;
				resolve();
			})
	});
	const countEZCards = new Promise((resolve, reject) => {
		cardModel.count({category: 'electoroZone'}, function (err, countElectroZoneCards) {
			if (err) {
				console.log('mistake in count electro cards', countElectroZoneCards);
				console.error(err);
				return reject(err);
			}
			cards.countElectroZoneCards = countElectroZoneCards;
			resolve();
		})
	});
	const SZCards = new Promise((resolve, reject) => {
		cardModel.find({category: 'stroyZone'}).sort({datePosted: -1}).skip(cardLimit).limit(skipCards)
			.exec(function (err, stroyZoneCards) {
				if (err) {
					console.log('mistake in stroy cards', stroyZoneCards);
					console.error(err);
					return reject(err);
				}
				cards.stroyZoneCards = stroyZoneCards;
				resolve();
			})
	});
	const countSZCards = new Promise((resolve, reject) => {
		cardModel.count({category: 'stroyZone'}, function (err, countStroyZoneCards) {
			if (err) {
				console.log('mistake in count stroy cards', countStroyZoneCards);
				console.error(err);
				return reject(err);
			}
			cards.countStroyZoneCards = countStroyZoneCards;
			resolve();
		})
	});

	Promise.all([HZCards, countHZCards, EZCards, countEZCards, SZCards, countSZCards]).then(value => {
		return res.json({cards});
	}, err => {
		console.log(err)
		next(err)
	});

};

module.exports.redirectToUserCard = function (req, res, next) {
	let {idUser, idCard} = req.body;
	// idCard = mongoose.Types.ObjectId.fromString(idCard);
	new Promise((resolve, reject) => {
		cardModel.findById(idCard)
			.exec(function (err, card) {
				if (err) {
					// console.error(err);
					return reject(err);
				}

				if (!card) {
					return reject(new Error('There is no card found.'));
				}
				// console.log('before culk',countCard);
				// countCard= countCard %10?(Math.round(countCard/10)+1)*10:countCard;
				// console.log('after culk',countCard);
				resolve(card);
			})
	})
		.then((card) => {
			return new Promise((resolve, reject) => {
				cardModel.find({idUser: idUser, datePosted: {$gte: card.datePosted}}).sort({datePosted: -1})
					.exec(function (err, cards) {
						if (err) {
							// console.error(err);
							return reject(err);
						}
						resolve(cards);
					})
			})
		})
		.then((cards) => {
			// console.log(6543, cards);
			console.log('this is id', idCard);
			console.log(`\n\n=>>`, cards)
			return res.json({cards});
		})
		.catch(err => {
			console.log(err);
			next(err)
		})
}
