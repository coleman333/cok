const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const forumSchema = new Schema({
	avatar: {type: String, required: false},
	idUser: {type: String, required: true},
	firstName: {type: String, required: false},
	organisation: {type: String, required: false},
  title: {type: String, required: false},
	datePosted:{type:Date,required:true},
	category:{type:String,required:false},
	BadgeAmountMessages:{type:String,required:false},
	idMessages:{type:String,required:false},
	role:{type:String,required:false}
});

module.exports = mongoose.model('Forum', forumSchema);