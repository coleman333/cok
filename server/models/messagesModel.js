const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const forumSchema = new Schema({
	avatar: {type: String, required: false},
	idUser: {type: String, required: true},
	firstName: {type: String, required: false},
	organisation: {type: String, required: false},
	text: {type: String, required: false},
	datePosted:{type:Date,required:true},
	category:{type:String,required:false},
	file:{type:String, required:false},
	fileType:{type:String, required:false}
});

module.exports = mongoose.model('Forum', forumSchema);